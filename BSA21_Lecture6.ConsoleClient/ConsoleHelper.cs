﻿using System;

namespace BSA21_Lecture6.ConsoleClient
{
    public static class ConsoleHelper
    {
        const string lockObject = "console";
        public static void PrintLine(this string text, ConsoleColor color = ConsoleColor.Gray)
        {
            lock (lockObject)
            {
                Console.ForegroundColor = color;
                Console.WriteLine(text);
                Console.ResetColor();
            }
        }
        public static void PrintLine(this string text, int color)
        {
            PrintLine(text, (ConsoleColor)color);
        }
        public static void Print(this string text, ConsoleColor color = ConsoleColor.Gray)
        {
            lock (lockObject)
            {
                Console.ForegroundColor = color;
                Console.Write(text);
                Console.ResetColor();
            }
        }
        public static void Print(this string text, int color)
        {
            Print(text, (ConsoleColor)color);
        }
    }
}
