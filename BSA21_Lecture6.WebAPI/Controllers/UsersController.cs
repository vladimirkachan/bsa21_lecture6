﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BSA21_Lecture6.BLL.Interfaces;
using BSA21_Lecture6.Common.DTO;
using BSA21_Lecture6.DAL.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Task = BSA21_Lecture6.DAL.Entities.Task;

namespace BSA21_Lecture6.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        readonly IService<User, UserDTO> userService;
        readonly IService<Task, TaskDTO> taskService;
        readonly IService<Project, ProjectDTO> projectService;
        readonly IValidator<UserCreateDTO> validator;

        public UsersController(IService<User, UserDTO> userService, 
                               IService<Task,TaskDTO> taskService,
                               IService<Project, ProjectDTO> projectService,
                               IValidator<UserCreateDTO> validator)
        {
            this.userService = userService;
            this.taskService = taskService;
            this.projectService = projectService;
            this.validator = validator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> Get()
        {
            return Ok(await userService.Get());
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<UserDTO>> Get(int id)
        {
            var item = await userService.Get(id);
            return item == null ? NotFound($"There is no such user with Id = {id}") : Ok(item);
        }

        [HttpPost]
        public async Task<ActionResult<UserDTO>> Post([FromBody] UserCreateDTO dto)
        {
            var validationResult = await validator.ValidateAsync(dto);
            if (!validationResult.IsValid) return BadRequest(validationResult.Errors.First().ToString());
            var value = await userService.Create(dto);
            return CreatedAtAction("POST", value);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<UserDTO>> Put(int id, [FromBody] UserCreateDTO dto)
        {
            var validationResult = await validator.ValidateAsync(dto);
            if (!validationResult.IsValid) return BadRequest(validationResult.Errors.First().ToString());
            dto.Id = id;
            var updated = await userService.Update(dto);
            return updated ? Ok(await userService.Get(id)) : NotFound($"No user with ID {id}");
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await userService.Delete(id);
            return deleted ? NoContent() : NotFound($"No user with ID {id}");
        }

        [HttpGet("sortedByNameAndTaskNameLength")]
        public async Task<ActionResult<IEnumerable<KeyValuePair<UserDTO, IEnumerable<TaskDTO>>>>> GetSortedUsers()
        {
            var users = await userService.Get();
            var query = from user in users
                        orderby user.FirstName, user.LastName
                        select KeyValuePair.Create(user, 
                                                   from task in taskService.Get(t => t.PerformerId == user.Id).Result
                                                   orderby task.Name.Length, task.Name.ToUpper()
                                                   select task);
            return Ok(query.AsEnumerable());
        }

        [HttpGet("custom/{userId:int}")]
        public async Task<ActionResult<CustomUserDTO>> GetUserObject(int userId)
        {
            var user = await userService.Get(userId);
            int? teamId = (await userService.GetEntity(userId)).TeamId;
            CustomUserDTO customUser;
            if (teamId == null) customUser = new CustomUserDTO {User = user};
            else
            {
                var projects = await projectService.Get(pe => pe.TeamId == teamId);
                var projectsQuery = from p in projects
                                    orderby p.CreatedAt
                                    select p;
                var lastProject = projectsQuery.LastOrDefault();
                var totalTasks = await taskService.Get(te => te.ProjectId == lastProject.Id);
                int totalTaskUnderLastProject = totalTasks.Count();
                var unperformedTasks = await taskService.Get(te => te.PerformerId == userId &&
                                                             (te.FinishedAt == null || te.FinishedAt > DateTime.Now));
                int totalUnperformedTask = unperformedTasks.Count();
                var tasksByUser = await taskService.Get(te => te.PerformerId == userId);
                var longestTaskQuery = from t in tasksByUser
                                       orderby (t.FinishedAt ?? DateTime.Now) - t.CreatedAt select t;
                var longestTask = longestTaskQuery.LastOrDefault();
                customUser = new CustomUserDTO
                {
                    User = user,
                    LastProject = lastProject,
                    TotalTaskUnderLastProject = totalTaskUnderLastProject,
                    TotalUnperformedTask = totalUnperformedTask,
                    LongestTask = longestTask
                };
            }
            return Ok(customUser);
        }

    }
}
