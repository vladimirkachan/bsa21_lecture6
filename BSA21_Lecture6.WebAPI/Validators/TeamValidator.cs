﻿using BSA21_Lecture6.Common.DTO;
using FluentValidation;

namespace BSA21_Lecture6.WebAPI.Validators
{
    public class TeamValidator : AbstractValidator<TeamCreateDTO> 
    {
        public TeamValidator()
        {
            RuleFor(i => i.Name).NotEmpty().Length(2, 100).WithMessage("Team name length must be 2-100");
        }
    }
}
