﻿using BSA21_Lecture6.Common.DTO;
using FluentValidation;

namespace BSA21_Lecture6.WebAPI.Validators
{
    public class ProjectValidator : AbstractValidator<ProjectCreateDTO>
    {
        public ProjectValidator()
        {
            RuleFor(i => i.Name).NotEmpty().Length(2, 100).WithMessage("Project name length must be 2-100");
            RuleFor(i => i.Description).Length(0, 200).WithMessage("Project description length must be 0-200");
        }
    }
}
