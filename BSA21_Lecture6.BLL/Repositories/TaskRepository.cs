﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BSA21_Lecture6.DAL.Context;
using BSA21_Lecture6.DAL.Entities;

namespace BSA21_Lecture6.BLL.Repositories
{
    public sealed class TaskRepository : BaseRepository<Task>
    {
        public TaskRepository(DataContext context) : base(context) {}

        public override async System.Threading.Tasks.Task<IEnumerable<Task>> Get(Expression<Func<Task, bool>> filter = null)
        {
            if (filter == null) return await System.Threading.Tasks.Task.Run(db.Tasks.AsEnumerable);
            var f = filter.Compile();
            var query = from i in db.Tasks.AsEnumerable()
                        where f(i)
                        select i;
            return query.AsEnumerable();
        }
        public override async System.Threading.Tasks.Task<Task> Get(int id)
        {
            return await db.Tasks.FindAsync(id);
        }
        public override async System.Threading.Tasks.Task<Task> Create(Task input)
        {
            var value = await db.Tasks.AddAsync(input);
            await db.SaveChangesAsync();
            return value.Entity;
        }
        public override async System.Threading.Tasks.Task<bool> Update(Task input)
        {
            var item = await Get(input.Id);
            if (item == null) return false;
            item.Description = input.Description;
            item.FinishedAt = input.FinishedAt;
            item.Name = input.Name;
            await db.SaveChangesAsync();
            return true;
        }
        public override async System.Threading.Tasks.Task<bool> Delete(int id)
        {
            var item = await Get(id);
            if (item == null) return false;
            db.Tasks.Remove(item);
            await db.SaveChangesAsync();
            return true;
        }

    }
}
