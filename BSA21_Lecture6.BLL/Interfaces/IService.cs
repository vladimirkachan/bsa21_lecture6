﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BSA21_Lecture6.BLL.Interfaces
{
    public interface IService<TEntity, TDto>
    {
        Task<IEnumerable<TDto>> Get(Expression<Func<TEntity, bool>> filter = null);
        Task<TDto> Get(int id);
        Task<TDto> Create(TDto dto);
        Task<bool> Update(TDto dto);
        Task<bool> Delete(int id);
        Task<TEntity> GetEntity(int id);
    }
}
