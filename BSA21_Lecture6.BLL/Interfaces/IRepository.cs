﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BSA21_Lecture6.BLL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> Get(Expression<Func<TEntity, bool>> filter = null);
        Task<TEntity> Get(int id); 
        Task<TEntity> Create(TEntity input);
        Task<bool> Update(TEntity input);
        Task<bool> Delete(int id);
    }
}
