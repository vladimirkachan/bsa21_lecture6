﻿using BSA21_Lecture6.DAL.Entities;
using Task = BSA21_Lecture6.DAL.Entities.Task;

namespace BSA21_Lecture6.BLL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Project> Projects { get; }
        IRepository<Task> Tasks { get; }
        IRepository<User> Users { get; }
        IRepository<Team> Teams { get; }

    }
}
