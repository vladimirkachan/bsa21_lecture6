﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using BSA21_Lecture6.BLL.Interfaces;
using BSA21_Lecture6.Common.DTO;
using BSA21_Lecture6.DAL.Entities;

namespace BSA21_Lecture6.BLL.Services
{
    public class UserService : IService<User, UserDTO>
    {
        readonly IRepository<User> repository;
        readonly IMapper mapper;

        public UserService(IRepository<User> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        public async Task< IEnumerable<UserDTO>> Get(Expression<Func<User, bool>> filter = null)
        {
            return mapper.Map<IEnumerable<UserDTO>>(await repository.Get(filter));
        }
        public async Task< UserDTO> Get(int id)
        {
            var entity = await repository.Get(id);
            return entity == null ? null : mapper.Map<UserDTO>(entity);
        }
        public async Task< UserDTO> Create(UserDTO dto)
        {
            dto.RegisteredAt = DateTime.Now;
            var entity = mapper.Map<User>(dto);
            var user = await repository.Create(entity);
            return mapper.Map<UserDTO>(user);
        }
        public async Task< bool> Update(UserDTO dto)
        {
            return await repository.Update(mapper.Map<User>(dto));
        }
        public async Task< bool> Delete(int id)
        {
            return await repository.Delete(id);
        }
        public async Task< User> GetEntity(int id)
        {
            return await repository.Get(id);
        }
    }
}
