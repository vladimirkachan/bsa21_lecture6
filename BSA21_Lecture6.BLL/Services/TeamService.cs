﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using BSA21_Lecture6.BLL.Interfaces;
using BSA21_Lecture6.Common.DTO;
using BSA21_Lecture6.DAL.Entities;

namespace BSA21_Lecture6.BLL.Services
{
    public class TeamService : IService<Team, TeamDTO>
    {
        readonly IRepository<Team> repository;
        readonly IMapper mapper;

        public TeamService(IRepository<Team> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public async Task< IEnumerable<TeamDTO>> Get(Expression<Func<Team, bool>> filter = null)
        {
            return mapper.Map<IEnumerable<TeamDTO>>(await repository.Get(filter));
        }
        public async Task< TeamDTO> Get(int id)
        {
            var entity = await repository.Get(id);
            return entity == null ? null : mapper.Map<TeamDTO>(entity);
        }
        public async Task< TeamDTO> Create(TeamDTO dto)
        {
            dto.CreatedAt = DateTime.Now;
            var entity = mapper.Map<Team>(dto);
            var team = await repository.Create(entity);
            return mapper.Map<TeamDTO>(team);
        }
        public async Task< bool> Update(TeamDTO dto)
        {
            return await repository.Update(mapper.Map<Team>(dto));
        }
        public async Task< bool> Delete(int id)
        {
            return await repository.Delete(id);
        }
        public async Task< Team> GetEntity(int id)
        {
            return await repository.Get(id);
        }
    }
}
