﻿using Newtonsoft.Json;

namespace BSA21_Lecture6.DAL.Entities
{
    public abstract class BaseEntity 
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
