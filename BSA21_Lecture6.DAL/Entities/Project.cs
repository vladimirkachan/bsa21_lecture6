﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace BSA21_Lecture6.DAL.Entities
{
    public class Project : BaseEntity
    {
        [JsonProperty("authorId")]
        public int AuthorId { get; set; }

        [JsonProperty("teamId")]
        public int TeamId { get; set; }

        [JsonProperty("name")]
        [MinLength(2)]
        [MaxLength(100)]
        public string Name { get; set; }

        [JsonProperty("description")]
        [MaxLength(200)]
        public string Description { get; set; }

        [JsonProperty("deadline")]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime Deadline { get; set; }

        [JsonProperty("createdAt")]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime CreatedAt { get; set; }

        [JsonIgnore]
        public User User { get; set; }

        [JsonIgnore]
        public Team Team { get; set; }
    }
}
