﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace BSA21_Lecture6.DAL.Entities
{
    public class User : BaseEntity
    {
        [JsonProperty("teamId")]
        public int? TeamId { get; set; }

        [JsonProperty("firstName")]
        [MinLength(2)]
        [MaxLength(20)]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        [MinLength(2)]
        [MaxLength(25)]
        public string LastName { get; set; }

        [JsonProperty("email")]
        [MinLength(5)]
        [MaxLength(100)]
        public string Email { get; set; }

        [JsonProperty("registeredAt")]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime RegisteredAt { get; set; }

        [JsonProperty("birthDay")]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime BirthDay { get; set; }

        [JsonIgnore]
        public Team Team { get; set; }

    }
}
