﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace BSA21_Lecture6.DAL.Entities
{
    public class Task : BaseEntity
    {
        [JsonProperty("projectId")]
        public int ProjectId { get; set; }

        [JsonProperty("performerId")]
        public int PerformerId { get; set; }

        [JsonProperty("name")]
        [MinLength(2)]
        [MaxLength(100)]
        public string Name { get; set; }

        [JsonProperty("description")]
        [MaxLength(200)]
        public string Description { get; set; }

        [JsonProperty("state")]
        public int State { get; set; }

        [JsonProperty("createdAt")]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("finishedAt")]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime? FinishedAt { get; set; }

        [JsonIgnore] public Project Project { get; set; }
        [JsonIgnore] public User User { get; set; }

    }
}
