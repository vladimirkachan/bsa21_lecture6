﻿namespace BSA21_Lecture6.Common.DTO
{
    public class CustomUserDTO
    {
        public UserDTO User {get; set;}
        public ProjectDTO LastProject {get; set;}
        public int? TotalTaskUnderLastProject {get; set;}
        public int? TotalUnperformedTask { get; set;}
        public TaskDTO LongestTask {get; set;}
    }
}
