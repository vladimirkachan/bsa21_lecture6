﻿using System;
using System.Text.Json.Serialization;

namespace BSA21_Lecture6.Common.DTO
{
    public class TeamCreateDTO : TeamDTO
    {
        [JsonIgnore]
        public override int Id { get => base.Id; set => base.Id = value; }
        [JsonIgnore]
        public override DateTime CreatedAt { get => base.CreatedAt; set => base.CreatedAt = value; }
    }
}
