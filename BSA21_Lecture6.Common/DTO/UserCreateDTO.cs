﻿using System;
using System.Text.Json.Serialization;

namespace BSA21_Lecture6.Common.DTO
{
    public class UserCreateDTO : UserDTO
    {
        [JsonIgnore] public override int Id { get => base.Id; set => base.Id = value; }
        [JsonIgnore] public override DateTime RegisteredAt { get => base.RegisteredAt; set => base.RegisteredAt = value; }
    }
}
