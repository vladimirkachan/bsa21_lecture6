﻿using System;

namespace BSA21_Lecture6.Common.DTO
{
    public class ProjectDTO : BaseDTO
    {
        public string Name {get; set;}
        public string Description {get; set;}
        public DateTime Deadline {get; set;}
        public DateTime CreatedAt {get; set;}
    }
}
