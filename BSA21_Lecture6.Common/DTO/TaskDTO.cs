﻿using System;

namespace BSA21_Lecture6.Common.DTO
{
    public class TaskDTO : BaseDTO
    {
        public string Name {get; set;}
        public string Description {get; set;}
        public virtual int State {get; set;}
        public virtual DateTime CreatedAt {get; set;}
        public DateTime? FinishedAt {get; set;}
    }
}
