﻿using System;

namespace BSA21_Lecture6.Common.DTO
{
    public class UserDTO : BaseDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email {get; set;}
        public virtual DateTime RegisteredAt {get; set;}
        public DateTime BirthDay {get; set;}    
    }
}
