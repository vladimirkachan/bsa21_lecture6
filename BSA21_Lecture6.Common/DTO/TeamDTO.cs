﻿using System;

namespace BSA21_Lecture6.Common.DTO
{
    public class TeamDTO : BaseDTO
    {
        public string Name {get; set;}
        public virtual DateTime CreatedAt {get; set;} 
    }
}
