﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using BSA21_Lecture6.Common;
using BSA21_Lecture6.DAL;

namespace BSA21_Lecture6.DesktopApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            dataGridView1.DataSource = bindingSource1;
            dataGridView2.DataSource = bindingSource2;
            dataGridView3.DataSource = bindingSource3;
            dataGridView4.DataSource = bindingSource4;
        }

        protected override async void OnLoad(EventArgs e)
        {
            
            Cursor = Cursors.WaitCursor;
            await LoadThrowWebAPI();
            Cursor = Cursors.Default;
        }
        async Task LoadThrowWebAPI()
        {
            bindingSource1.DataSource = await EndPoints.GetProjects();
            bindingSource2.DataSource = await EndPoints.GetTasks();
            bindingSource3.DataSource = await EndPoints.GetTeams();
            bindingSource4.DataSource = await EndPoints.GetUsers();
        }
        //async Task LoadFromDb()
        //{
        //    await using DataContext db = new();
        //    await db.Projects.LoadAsync();
        //    bindingSource1.DataSource = db.Projects.Local.ToBindingList();
        //    await db.Tasks.LoadAsync();
        //    bindingSource2.DataSource = db.Tasks.Local.ToBindingList();
        //    await db.Teams.LoadAsync();
        //    bindingSource3.DataSource = db.Teams.Local.ToBindingList();
        //    await db.Users.LoadAsync();
        //    bindingSource4.DataSource = db.Users.Local.ToBindingList();
        //}
        async Task LoadFromBsa()
        {
            bindingSource1.DataSource = await Requests.GetProjects();
            bindingSource2.DataSource = await Requests.GetTasks();
            bindingSource3.DataSource = await Requests.GetTeams();
            bindingSource4.DataSource = await Requests.GetUsers();
        }
        private async void load1_Click(object sender, EventArgs e)
        {
            await LoadThrowWebAPI();
        }

        private void close1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
